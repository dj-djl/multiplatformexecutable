﻿#!/bin/sh
#Remove the MZ Error if possible - otherwise a message is shown.
COLUMNS=$(tput cols)
#echo $COLUMNS
echo "Ignore any \"MZ not found\" Error"
#echo -en "  " #$Columns is too much by 1 - these 2 spaces counter that ;)
for i in `seq 1 $COLUMNS
 seq 1 $COLUMNS`
 do
echo -en "\b"
done
for i in `seq 1 $COLUMNS
 seq 1 $COLUMNS`
 do
echo -en " "
done
for i in `seq 1 $COLUMNS
 seq 1 $COLUMNS`
 do
echo -en "\b"
done
 # Overwrite the blank space with a header message
 echo -e "DJL's Multi platform Executable"
 echo -e "GPL 2  - 2010 - http://www.dj-djl.com"
  
TAR={{TarOffset}}
  
#Begin ELF extraction
MKTEMP=`whereis mktemp tmpfile | head -n 1 | sed "s/.*\?: //"|sed "s/ .*//"`
TEMPELF=`$MKTEMP`
#case $HOSTTYPE in 
#x86_64) ELF=32768
# ;; 
#esac
tail -c +$TAR $DJLMZ | tar -tv $HOSTTYPE 2>/dev/null
if [ $? = 0 ]
then
	tail -c +$TAR $DJLMZ | tar -xO $HOSTTYPE > $TEMPELF  2>/dev/null
else
	#look to see if we are on a console
	DIALOG="echo "
	if [ $DISPLAY = "" ]
	then
		DIALOG="#"
	else
		if command -v kdialog > /dev/null
		then
			DIALOG="kdialog --sorry "
		else 
			if command -v zenity > /dev/null
			then
				DIALOG="zenity --warning --text="
			else
				DIALOG="xdialog "
			fi
		fi
	fi
	echo -e "Unfortunatly your platform '$HOSTTYPE' Is not supported by this application.\nPlease contact the applciations author and ask them to support your platform"
	$DIALOG "Unfortunatly your platform '$HOSTTYPE' Is not supported by this application.\nPlease contact the applciations author and ask them to support your platform"
	exit
fi  
#echo $DJLMZ; dd if="$DJLMZ" of="$TEMPELF" bs=$ELF skip=1
chmod +x $TEMPELF
$TEMPELF
sleep 1
rm $TEMPELF

exit



#these blank lines are neccassary to stop sh crashing when it hits the binary data - do not remove them!


