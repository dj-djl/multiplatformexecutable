﻿Public Class Form1

    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rdoDotNet.CheckedChanged
        tlpDotNet.Enabled = rdoDotNet.Checked
        tlpNative.Enabled = rdoNative.Checked

    End Sub
    Private ReadOnly Property MonoStub() As String
        Get
            Return New System.IO.StreamReader(Me.GetType.Assembly.GetManifestResourceStream("MultiPlatformExecutable.MonoStub.sh")).ReadToEnd
        End Get
    End Property
    Private ReadOnly Property BinariesStub() As String
        Get
            Return New System.IO.StreamReader(Me.GetType.Assembly.GetManifestResourceStream("MultiPlatformExecutable.BinariesStub.sh")).ReadToEnd
        End Get
    End Property
    Private ReadOnly Property MZStub() As Byte()
        Get
            Static bytMZ As Object = Nothing
            If bytMZ Is Nothing Then
                Dim stMZ As System.IO.Stream = Me.GetType.Assembly.GetManifestResourceStream("MultiPlatformExecutable.MZStub.hex")
                Dim bytMZ1(stMZ.Length - 1) As Byte
                stMZ.Read(bytMZ1, 0, stMZ.Length)
                bytMZ = bytMZ1
            End If
            Return bytMZ
        End Get
    End Property
    Private Sub Button4_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button4.Click
        Dim sfd As New SaveFileDialog
        sfd.Title = "Save EXE File"
        If rdoDotNet.Checked Then
            sfd.FileName = txtDotNet.Text.Replace(".exe", "") & ".MultiPlatform.exe"
        Else
            sfd.FileName = txtMZ.Text.Replace(".exe", "") & ".MultiPlatform.exe"
        End If
        sfd.Filter = "EXE Files|*.exe|All Files|*.*"
        sfd.OverwritePrompt = True
        If sfd.ShowDialog <> Windows.Forms.DialogResult.OK Then
            Return
        End If
        If rdoDotNet.Checked Then
            CreateMZ(sfd.FileName, txtDotNet.Text, Nothing, MonoStub)
        ElseIf rdoNative.Checked Then
            Dim lstOtherBinaries As New SortedList(Of String, Byte())
            If txtNix32.Text <> "" Then
                lstOtherBinaries.Add("x86", System.IO.File.ReadAllBytes(txtNix32.Text))
            End If
            If txtNix64.Text <> "" Then
                lstOtherBinaries.Add("x86_64", System.IO.File.ReadAllBytes(txtNix64.Text))
            End If
            CreateMZ(sfd.FileName, txtMZ.Text, lstOtherBinaries, BinariesStub)
        End If
    End Sub
    Private Sub CreateMZ(ByVal strOut As String, ByVal strMZBase As String, ByVal slOtherBinaries As SortedList(Of String, Byte()), ByVal strPrimarySH As String)
        Dim fleMZBase As New System.IO.FileStream(strMZBase, IO.FileMode.Open, IO.FileAccess.Read)
        strPrimarySH = strPrimarySH.Replace(vbCrLf, vbLf).Replace(vbCr, vbLf)

        Dim fleOut As New System.IO.FileStream(strOut, IO.FileMode.Create, IO.FileAccess.Write)
        Dim bwOut As New System.IO.BinaryWriter(fleOut)
        bwOut.Write(CByte(&H4D)) 'M
        bwOut.Write(CByte(&H5A)) 'Z
        bwOut.Write(CByte(&HA)) '(LF)
        Dim intPrimarySHOffset As Integer = (fleMZBase.Length + 1024)  'leave one entirely empty block then start at the next one.
        intPrimarySHOffset = intPrimarySHOffset - (intPrimarySHOffset Mod 512)
        Dim strSHStub As String = "export DJLMZ=""$0""" & vbLf & _
                    "tail ""$0"" -c +" & (intPrimarySHOffset + 1) & "|sh" & vbLf & _
                    "exit" & vbLf
        If strSHStub.Length > &H3C - 3 Then
            MsgBox("Unfortunatly the SH stub for Linux would be too long - probably the Base EXE file is humoungous?")
        End If
        bwOut.Write(System.Text.Encoding.ASCII.GetBytes(strSHStub))
        bwOut.Seek(&H3C, IO.SeekOrigin.Begin)
        fleMZBase.Seek(&H3C, IO.SeekOrigin.Begin)
        'copy the entire MZ
        Dim bytBuffer(1023) As Byte
        Dim intBytesRead As Integer = fleMZBase.Read(bytBuffer, 0, 1024)
        While intBytesRead > 0
            bwOut.Write(bytBuffer, 0, intBytesRead)
            intBytesRead = fleMZBase.Read(bytBuffer, 0, 1024)
        End While
        bwOut.Seek(intPrimarySHOffset, IO.SeekOrigin.Begin)
        Dim intTarOffset As Integer = intPrimarySHOffset + strPrimarySH.Length + 1024 '1024 to leave plenty of room for the offset itself. and move into the next block.
        intTarOffset = intTarOffset - (intTarOffset Mod 512)
        bwOut.Write(System.Text.Encoding.ASCII.GetBytes(strPrimarySH.Replace("{{TarOffset}}", intTarOffset + 1)))
        bwOut.Flush()
        bwOut.Seek(intTarOffset, IO.SeekOrigin.Begin) 'this is safe - if the list is empty nothing gets written so the file doesn't grow to this point.
        Static bytNULLs(511) As Byte
        If slOtherBinaries IsNot Nothing Then
            For Each kvpOtherBinary As KeyValuePair(Of String, Byte()) In slOtherBinaries
                bwOut.Write(GetTarHeader(kvpOtherBinary.Key, kvpOtherBinary.Value.Length))
                bwOut.Write(kvpOtherBinary.Value)
                bwOut.Write(bytNULLs, 0, 511 - ((kvpOtherBinary.Value.Length - 1) Mod 512) Mod 512)
                bwOut.Flush()
            Next
        End If

        Dim lngCustomOffset As Long = 0
        Dim lngEndOfCustomFile As Long = 0
        If txtCustomResource.Text.Trim <> "" AndAlso System.IO.File.Exists(txtCustomResource.Text) Then
            bwOut.Write(GetTarHeader("CustomResource.dat", New System.IO.FileInfo(txtCustomResource.Text).Length))
            bwOut.Flush()
            lngCustomOffset = bwOut.BaseStream.Length
            Dim fleCustom As New System.IO.FileStream(txtCustomResource.Text, IO.FileMode.Open, IO.FileAccess.Read)
            intBytesRead = fleCustom.Read(bytBuffer, 0, 1024)
            While intBytesRead > 0
                bwOut.Write(bytBuffer, 0, intBytesRead)
                intBytesRead = fleCustom.Read(bytBuffer, 0, 1024)
            End While
            bwOut.Flush()
            lngEndOfCustomFile = bwOut.BaseStream.Length
            bwOut.Write(bytNULLs, 0, 511 - ((New System.IO.FileInfo(txtCustomResource.Text).Length - 1) Mod 512) Mod 512)
            bwOut.Flush()
        End If
        bwOut.Write(GetTarHeader("CustomResource.info", 512))
        bwOut.Flush()
        Dim lngCustomLength As Long = 0
        If txtCustomResource.Text.Trim <> "" AndAlso System.IO.File.Exists(txtCustomResource.Text) Then
            lngCustomLength = (New System.IO.FileInfo(txtCustomResource.Text)).Length
        Else
            lngCustomOffset = bwOut.BaseStream.Length + 24
            lngEndOfCustomFile = lngCustomOffset
        End If
        bwOut.Seek(512 - 24, IO.SeekOrigin.Current)
        bwOut.Write(lngCustomOffset)
        bwOut.Write(lngCustomLength)
        bwOut.Flush()
        bwOut.Write(CLng(lngEndOfCustomFile - (bwOut.BaseStream.Length + 8)))
        bwOut.Close()
        MsgBox("Done!")
    End Sub
    Private Function GetTarHeader(ByVal strFilename As String, ByVal intFileSize As Long) As Byte()
        Dim bytTarHEader(511) As Byte
        Dim mstTarHeader As New System.IO.MemoryStream(bytTarHEader)
        Dim bwTarHEader As New System.IO.BinaryWriter(mstTarHeader)
        bwTarHEader.Write(System.Text.Encoding.ASCII.GetBytes(strFilename))
        bwTarHEader.Seek(100, IO.SeekOrigin.Begin)
        bwTarHEader.Write(System.Text.Encoding.ASCII.GetBytes("0000700"))
        bwTarHEader.Write(CByte(0))
        bwTarHEader.Write(System.Text.Encoding.ASCII.GetBytes("0000000"))
        bwTarHEader.Write(CByte(0))
        bwTarHEader.Write(System.Text.Encoding.ASCII.GetBytes("0000000"))
        bwTarHEader.Write(CByte(0))
        bwTarHEader.Write(System.Text.Encoding.ASCII.GetBytes(Oct(intFileSize).PadLeft(11, "0")))
        bwTarHEader.Write(CByte(0))
        bwTarHEader.Write(System.Text.Encoding.ASCII.GetBytes(Oct(Now.Subtract("1 Jan 1970").TotalSeconds).PadLeft(11, "0")))
        bwTarHEader.Write(CByte(0))
        bwTarHEader.Write(System.Text.Encoding.ASCII.GetBytes("        "))
        bwTarHEader.Write(System.Text.Encoding.ASCII.GetBytes("0"))
        bwTarHEader.Flush()

        Dim intCheckSum As Integer = 0
        For Each bytByte As Byte In bytTarHEader
            intCheckSum += bytByte
        Next
        bwTarHEader.Seek(148, IO.SeekOrigin.Begin)
        bwTarHEader.Write(System.Text.Encoding.ASCII.GetBytes(Oct(intCheckSum).PadLeft(6, "0")))
        bwTarHEader.Write(CByte(0))

        Return bytTarHEader
    End Function

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click
        Browse("Select mono binary", "EXE Filess|*.exe|All Files|*.*", txtDotNet)
    End Sub
    Private Sub Browse(ByVal strTitle As String, ByVal strFilter As String, ByVal txtFilename As TextBox)
        Dim ofd As New OpenFileDialog
        ofd.Title = strTitle
        ofd.Filter = strFilter
        ofd.CheckFileExists = True
        If ofd.ShowDialog = Windows.Forms.DialogResult.OK Then
            txtFilename.Text = ofd.FileName
        End If
    End Sub
    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Browse("Select Native Windows Binary", "Exe Files|*.exe|All Files|*.*", txtMZ)
    End Sub

    Private Sub Button3_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button3.Click
        Browse("Select Native *nix 32 bit Binary", "All Files|*.*|Elf Files|*.elf|Out Files|*.out", txtNix32)
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        Browse("Select Native *nix 64 bit Binary", "All Files|*.*|Elf Files|*.elf|Out Files|*.out", txtNix64)
    End Sub

    Private Sub btnCustomResource_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCustomResource.Click
        Browse("Select Custom Resource File", "All Files|*.*", txtCustomResource)

    End Sub
    Private Sub Button7_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button7.Click, Button6.Click, Button8.Click
        Dim btnSender As Button = sender
        Dim strCode As String = GetCodeSample("GetResources", btnSender.Tag, True)
        If strCode IsNot Nothing Then
            Clipboard.SetText(strCode)
            MsgBox("Code sample has been copied to the clipboard")
        End If
    End Sub
    Public Function GetCodeSample(ByVal strName As String, ByVal strLanguage As String, ByVal flgShowMessages As Boolean) As String
        Static xmdCodeSamples As System.Xml.XmlDocument
        If xmdCodeSamples Is Nothing Then
            xmdCodeSamples = New System.Xml.XmlDocument
            xmdCodeSamples.Load(Me.GetType.Assembly.GetManifestResourceStream("MultiPlatformExecutable.CodeSamples.xml"))
        End If
        Dim xmeSample As System.Xml.XmlElement = xmdCodeSamples.SelectSingleNode("/CodeSamples/CodeSample[@Name='" & strName & "']")
        If xmeSample Is Nothing Then
            If flgShowMessages Then
                MsgBox("Code sample for """ & strName & """ not found!")
            End If
            Return Nothing
        End If
        xmeSample = xmeSample.SelectSingleNode("Code[@Language='" & strLanguage & "']")
        If xmeSample Is Nothing Then
            If flgShowMessages Then
                MsgBox(strLanguage & " Code sample for """ & strName & """ not found.")
            End If
            Return Nothing
        End If
        Return xmeSample.InnerText
    End Function
End Class
