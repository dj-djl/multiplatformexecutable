﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.Label1 = New System.Windows.Forms.Label
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel
        Me.rdoDotNet = New System.Windows.Forms.RadioButton
        Me.rdoNative = New System.Windows.Forms.RadioButton
        Me.tlpNative = New System.Windows.Forms.TableLayoutPanel
        Me.txtCustomResource = New System.Windows.Forms.TextBox
        Me.Label2 = New System.Windows.Forms.Label
        Me.Label3 = New System.Windows.Forms.Label
        Me.txtMZ = New System.Windows.Forms.TextBox
        Me.Button3 = New System.Windows.Forms.Button
        Me.txtNix32 = New System.Windows.Forms.TextBox
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button1 = New System.Windows.Forms.Button
        Me.txtNix64 = New System.Windows.Forms.TextBox
        Me.Label4 = New System.Windows.Forms.Label
        Me.btnCustomResource = New System.Windows.Forms.Button
        Me.Label5 = New System.Windows.Forms.Label
        Me.tlpDotNet = New System.Windows.Forms.TableLayoutPanel
        Me.txtDotNet = New System.Windows.Forms.TextBox
        Me.Button5 = New System.Windows.Forms.Button
        Me.Button4 = New System.Windows.Forms.Button
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.PictureBox2 = New System.Windows.Forms.PictureBox
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Label6 = New System.Windows.Forms.Label
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel
        Me.TableLayoutPanel3 = New System.Windows.Forms.TableLayoutPanel
        Me.Button6 = New System.Windows.Forms.Button
        Me.Button7 = New System.Windows.Forms.Button
        Me.Button8 = New System.Windows.Forms.Button
        Me.TableLayoutPanel1.SuspendLayout()
        Me.tlpNative.SuspendLayout()
        Me.tlpDotNet.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.TableLayoutPanel3.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.Dock = System.Windows.Forms.DockStyle.Top
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(0, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(702, 146)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = resources.GetString("Label1.Text")
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.AutoSize = True
        Me.TableLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.TableLayoutPanel1.ColumnCount = 3
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel1.Controls.Add(Me.rdoDotNet, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.rdoNative, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.tlpNative, 2, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.tlpDotNet, 2, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.Button4, 0, 2)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox1, 1, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.PictureBox2, 1, 1)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 146)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 4
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(702, 209)
        Me.TableLayoutPanel1.TabIndex = 1
        '
        'rdoDotNet
        '
        Me.rdoDotNet.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.rdoDotNet.AutoSize = True
        Me.rdoDotNet.Location = New System.Drawing.Point(3, 10)
        Me.rdoDotNet.Name = "rdoDotNet"
        Me.rdoDotNet.Size = New System.Drawing.Size(77, 17)
        Me.rdoDotNet.TabIndex = 0
        Me.rdoDotNet.TabStop = True
        Me.rdoDotNet.Text = ".Net Binary"
        Me.ToolTip1.SetToolTip(Me.rdoDotNet, resources.GetString("rdoDotNet.ToolTip"))
        Me.rdoDotNet.UseVisualStyleBackColor = True
        '
        'rdoNative
        '
        Me.rdoNative.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.rdoNative.AutoSize = True
        Me.rdoNative.Location = New System.Drawing.Point(3, 100)
        Me.rdoNative.Name = "rdoNative"
        Me.rdoNative.Size = New System.Drawing.Size(96, 17)
        Me.rdoNative.TabIndex = 1
        Me.rdoNative.TabStop = True
        Me.rdoNative.Text = "Native Binaries"
        Me.ToolTip1.SetToolTip(Me.rdoNative, resources.GetString("rdoNative.ToolTip"))
        Me.rdoNative.UseVisualStyleBackColor = True
        '
        'tlpNative
        '
        Me.tlpNative.AutoSize = True
        Me.tlpNative.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.tlpNative.ColumnCount = 3
        Me.tlpNative.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.tlpNative.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.tlpNative.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.tlpNative.Controls.Add(Me.txtCustomResource, 0, 3)
        Me.tlpNative.Controls.Add(Me.Label2, 0, 0)
        Me.tlpNative.Controls.Add(Me.Label3, 0, 1)
        Me.tlpNative.Controls.Add(Me.txtMZ, 1, 0)
        Me.tlpNative.Controls.Add(Me.Button3, 2, 1)
        Me.tlpNative.Controls.Add(Me.txtNix32, 1, 1)
        Me.tlpNative.Controls.Add(Me.Button2, 2, 0)
        Me.tlpNative.Controls.Add(Me.Button1, 2, 2)
        Me.tlpNative.Controls.Add(Me.txtNix64, 1, 2)
        Me.tlpNative.Controls.Add(Me.Label4, 0, 2)
        Me.tlpNative.Controls.Add(Me.btnCustomResource, 1, 3)
        Me.tlpNative.Controls.Add(Me.Label5, 0, 3)
        Me.tlpNative.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlpNative.Location = New System.Drawing.Point(143, 41)
        Me.tlpNative.Name = "tlpNative"
        Me.tlpNative.RowCount = 5
        Me.tlpNative.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.tlpNative.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.tlpNative.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.tlpNative.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.tlpNative.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.tlpNative.Size = New System.Drawing.Size(556, 136)
        Me.tlpNative.TabIndex = 2
        '
        'txtCustomResource
        '
        Me.txtCustomResource.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtCustomResource.Location = New System.Drawing.Point(100, 90)
        Me.txtCustomResource.Name = "txtCustomResource"
        Me.txtCustomResource.Size = New System.Drawing.Size(421, 20)
        Me.txtCustomResource.TabIndex = 10
        Me.ToolTip1.SetToolTip(Me.txtCustomResource, "Use the custom resource file to include resources that are common to all versions" & _
                " of the application - This saves including the resources in every file." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "See the" & _
                " note below about Resources")
        '
        'Label2
        '
        Me.Label2.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 8)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(51, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Windows"
        '
        'Label3
        '
        Me.Label3.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(3, 37)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(59, 13)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "*nix (32 bit)"
        '
        'txtMZ
        '
        Me.txtMZ.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtMZ.Location = New System.Drawing.Point(100, 3)
        Me.txtMZ.Name = "txtMZ"
        Me.txtMZ.Size = New System.Drawing.Size(421, 20)
        Me.txtMZ.TabIndex = 2
        '
        'Button3
        '
        Me.Button3.AutoSize = True
        Me.Button3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Button3.Location = New System.Drawing.Point(527, 32)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(26, 23)
        Me.Button3.TabIndex = 5
        Me.Button3.Text = "..."
        Me.Button3.UseVisualStyleBackColor = True
        '
        'txtNix32
        '
        Me.txtNix32.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtNix32.Location = New System.Drawing.Point(100, 32)
        Me.txtNix32.Name = "txtNix32"
        Me.txtNix32.Size = New System.Drawing.Size(421, 20)
        Me.txtNix32.TabIndex = 3
        '
        'Button2
        '
        Me.Button2.AutoSize = True
        Me.Button2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Button2.Location = New System.Drawing.Point(527, 3)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(26, 23)
        Me.Button2.TabIndex = 4
        Me.Button2.Text = "..."
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.AutoSize = True
        Me.Button1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Button1.Location = New System.Drawing.Point(527, 61)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(26, 23)
        Me.Button1.TabIndex = 8
        Me.Button1.Text = "..."
        Me.Button1.UseVisualStyleBackColor = True
        '
        'txtNix64
        '
        Me.txtNix64.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtNix64.Location = New System.Drawing.Point(100, 61)
        Me.txtNix64.Name = "txtNix64"
        Me.txtNix64.Size = New System.Drawing.Size(421, 20)
        Me.txtNix64.TabIndex = 7
        '
        'Label4
        '
        Me.Label4.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(3, 66)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(59, 13)
        Me.Label4.TabIndex = 6
        Me.Label4.Text = "*nix (64 bit)"
        '
        'btnCustomResource
        '
        Me.btnCustomResource.AutoSize = True
        Me.btnCustomResource.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.btnCustomResource.Location = New System.Drawing.Point(527, 90)
        Me.btnCustomResource.Name = "btnCustomResource"
        Me.btnCustomResource.Size = New System.Drawing.Size(26, 23)
        Me.btnCustomResource.TabIndex = 11
        Me.btnCustomResource.Text = "..."
        Me.ToolTip1.SetToolTip(Me.btnCustomResource, "Use the custom resource file to include resources that are common to all versions" & _
                " of the application - This saves including the resources in every file." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "See the" & _
                " note below about Resources")
        Me.btnCustomResource.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(3, 95)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(91, 13)
        Me.Label5.TabIndex = 9
        Me.Label5.Text = "Custom Resource"
        Me.ToolTip1.SetToolTip(Me.Label5, "Use the custom resource file to include resources that are common to all versions" & _
                " of the application - This saves including the resources in every file." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "See the" & _
                " note below about Resources")
        '
        'tlpDotNet
        '
        Me.tlpDotNet.AutoSize = True
        Me.tlpDotNet.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.tlpDotNet.ColumnCount = 3
        Me.tlpDotNet.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.tlpDotNet.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.tlpDotNet.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.tlpDotNet.Controls.Add(Me.txtDotNet, 1, 0)
        Me.tlpDotNet.Controls.Add(Me.Button5, 2, 0)
        Me.tlpDotNet.Dock = System.Windows.Forms.DockStyle.Fill
        Me.tlpDotNet.Location = New System.Drawing.Point(143, 3)
        Me.tlpDotNet.Name = "tlpDotNet"
        Me.tlpDotNet.RowCount = 1
        Me.tlpDotNet.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.tlpDotNet.Size = New System.Drawing.Size(556, 32)
        Me.tlpDotNet.TabIndex = 5
        '
        'txtDotNet
        '
        Me.txtDotNet.Dock = System.Windows.Forms.DockStyle.Fill
        Me.txtDotNet.Location = New System.Drawing.Point(3, 3)
        Me.txtDotNet.Name = "txtDotNet"
        Me.txtDotNet.Size = New System.Drawing.Size(518, 20)
        Me.txtDotNet.TabIndex = 2
        '
        'Button5
        '
        Me.Button5.AutoSize = True
        Me.Button5.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Button5.Location = New System.Drawing.Point(527, 3)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(26, 23)
        Me.Button5.TabIndex = 4
        Me.Button5.Text = "..."
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(3, 183)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(75, 23)
        Me.Button4.TabIndex = 6
        Me.Button4.Text = "Create"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(105, 3)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(32, 32)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox1.TabIndex = 7
        Me.PictureBox1.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox1, resources.GetString("PictureBox1.ToolTip"))
        '
        'PictureBox2
        '
        Me.PictureBox2.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.PictureBox2.Image = CType(resources.GetObject("PictureBox2.Image"), System.Drawing.Image)
        Me.PictureBox2.Location = New System.Drawing.Point(105, 93)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(32, 32)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox2.TabIndex = 8
        Me.PictureBox2.TabStop = False
        Me.ToolTip1.SetToolTip(Me.PictureBox2, resources.GetString("PictureBox2.ToolTip"))
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(3, 20)
        Me.Label6.Margin = New System.Windows.Forms.Padding(3, 20, 3, 20)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(509, 169)
        Me.Label6.TabIndex = 12
        Me.Label6.Text = resources.GetString("Label6.Text")
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.AutoSize = True
        Me.TableLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.TableLayoutPanel2.ColumnCount = 2
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me.TableLayoutPanel2.Controls.Add(Me.Label6, 0, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.TableLayoutPanel3, 1, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(0, 347)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(702, 209)
        Me.TableLayoutPanel2.TabIndex = 13
        '
        'TableLayoutPanel3
        '
        Me.TableLayoutPanel3.Anchor = System.Windows.Forms.AnchorStyles.Left
        Me.TableLayoutPanel3.AutoSize = True
        Me.TableLayoutPanel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.TableLayoutPanel3.ColumnCount = 1
        Me.TableLayoutPanel3.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me.TableLayoutPanel3.Controls.Add(Me.Button6, 0, 0)
        Me.TableLayoutPanel3.Controls.Add(Me.Button7, 0, 1)
        Me.TableLayoutPanel3.Controls.Add(Me.Button8, 0, 2)
        Me.TableLayoutPanel3.Location = New System.Drawing.Point(518, 41)
        Me.TableLayoutPanel3.Name = "TableLayoutPanel3"
        Me.TableLayoutPanel3.RowCount = 6
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel3.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20.0!))
        Me.TableLayoutPanel3.Size = New System.Drawing.Size(81, 127)
        Me.TableLayoutPanel3.TabIndex = 13
        '
        'Button6
        '
        Me.Button6.Location = New System.Drawing.Point(3, 3)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(75, 23)
        Me.Button6.TabIndex = 0
        Me.Button6.Tag = "C"
        Me.Button6.Text = "C/C++"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(3, 32)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(75, 23)
        Me.Button7.TabIndex = 1
        Me.Button7.Tag = "VBNet"
        Me.Button7.Text = "VB.NET"
        Me.Button7.UseVisualStyleBackColor = True
        '
        'Button8
        '
        Me.Button8.Location = New System.Drawing.Point(3, 61)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(75, 23)
        Me.Button8.TabIndex = 2
        Me.Button8.Tag = "C#"
        Me.Button8.Text = "C#"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(702, 556)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.TableLayoutPanel2)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.TableLayoutPanel1.PerformLayout()
        Me.tlpNative.ResumeLayout(False)
        Me.tlpNative.PerformLayout()
        Me.tlpDotNet.ResumeLayout(False)
        Me.tlpDotNet.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.TableLayoutPanel3.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel1 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents rdoDotNet As System.Windows.Forms.RadioButton
    Friend WithEvents rdoNative As System.Windows.Forms.RadioButton
    Friend WithEvents tlpNative As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents txtMZ As System.Windows.Forms.TextBox
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents txtNix32 As System.Windows.Forms.TextBox
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents tlpDotNet As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents txtDotNet As System.Windows.Forms.TextBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents txtNix64 As System.Windows.Forms.TextBox
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents txtCustomResource As System.Windows.Forms.TextBox
    Friend WithEvents btnCustomResource As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents TableLayoutPanel2 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents TableLayoutPanel3 As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents Button8 As System.Windows.Forms.Button

End Class
