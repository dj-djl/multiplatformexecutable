﻿#!/bin/sh
#Remove the MZ Error if possible - otherwise a message is shown.
COLUMNS=$(tput cols)
#echo $COLUMNS
echo "Ignore any \"MZ not found\" Error"
echo "  " #$Columns is too much by 1 - these 2 spaces counter that ;)
for i in `seq 1 $COLUMNS
 seq 1 $COLUMNS`
 do
echo -en "\b"
done
for i in `seq 1 $COLUMNS
 seq 1 $COLUMNS`
 do
echo -en " "
done
for i in `seq 1 $COLUMNS
 seq 1 $COLUMNS`
 do
echo -en "\b"
done
 # Overwrite the blank space with a header message
 echo -e "DJL's Multi platform Executable"
 echo -e "GPL 2  - 2010 - http://www.dj-djl.com"

#for .NET no extraction is necassary since mono uses the entire binary as-is
  #Check for mono
  if command -v mono &>/dev/null
  then                 
    mono $DJLMZ
  else                 
	  #look to see if we are on a console
	  DIALOG="echo "
	  if [ $DISPLAY = ""]
	  then
		DIALOG="#"
	  else
		if command -v	kdialog
		then
			DIALOG="kdialog --sorry "
		else 
			if command -v zenity
			then
				DIALOG="zenity --warning --text="
			else
				DIALOG="xdialog "
			fi
		fi
	  fi
	  $DIALOG "Unable to detect DotNet enviroment\nThis program uses DotNet - please install mono or an equivalant using your packagemanager"
	  echo Unable to detect DotNet enviroment\nThis program uses DotNet - please install mono or an equivalant using your packagemanager
  fi
#The exit statement is compulsorary to make sure we don't run into any binary data which might follow on.
exit
  
##Begin ELF extraction
#MKTEMP=`whereis mktemp tmpfile | head -n 1 | sed "s/.*\?: //"|sed "s/ .*//"`
#TEMPELF=`$MKTEMP`
# case $HOSTTYPE in 
#x86_64) ELF=32768
# ;; 
#esac
#echo $DJLMZ; dd if="$DJLMZ" of="$TEMPELF" bs=$ELF skip=1
#chmod +x $TEMPELF
# $TEMPELF
# rm $TEMPELF


